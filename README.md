Traza la ruta de menor coste utilizandolos  algoritmos A, voraz, profundidad, anchura y uniforme con la profundidad indicada. También hace uso de gnuplot con un pequeño script shell para pintar gráficas en base a los caminos solución.

Funciona con los mapas del Modelo Digital de Elevaciones - Instituto Geográfico Nacional (no incluidos en este repositorio) creando archivos HDF para hacer un uso más eficiente de los datos.

Asignatura Sistemas Inteligentes en la ESI UCLM.
Realizado junto a Pedro Romero.