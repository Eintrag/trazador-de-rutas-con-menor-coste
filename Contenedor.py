#coding:utf-8
from Nodo import Nodo
import math
#from heapq import * #Importamos colas con prioridad

class Contenedor:
  lista = []
  def getLen(self):
	  return len(self.lista)
  def add(self, x):
    self.lista.append(x)
    #heappush(self.priorQueue, x)
    '''
    i=0
    if len(self.lista)==0:
      self.lista.append(x)
    else:
      for y in self.lista:
        if y.valoracion < x.valoracion:
          self.lista.insert(i,x)
          break
    i+=1
    '''
  def get(self):
    numNodoMenorValor = -1
    nodoMenorValor = Nodo(None, None,0, float("inf"),0)
    for i in range(0, len(self.lista)):
      nodoActual = self.lista[i]
      if (nodoActual.valoracion < nodoMenorValor.valoracion):
        numNodoMenorValor = i
    return self.lista[numNodoMenorValor]
  
  def extraer(self):
    numNodoMenorValor = -1
    nodoMenorValor = Nodo(None, None,0, float("inf"),0)
    for i in range(0, len(self.lista)):
      nodoActual = self.lista[i]
      if (nodoActual.valoracion < nodoMenorValor.valoracion):
        nodoMenorValor=nodoActual
        numNodoMenorValor=i
    del self.lista[numNodoMenorValor]
    return nodoMenorValor
  
