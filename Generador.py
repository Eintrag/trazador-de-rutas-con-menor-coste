#coding:utf-8
from Nodo import Nodo
from Contenedor import Contenedor
from Estado import Estado
import random
import time
from LugarGeografico import LugarGeografico
import sys

def main(argv):
  contenedor = Contenedor()
  profundidad = 0
  lInicio = LugarGeografico(397388, 4353788)
  eInicio = Estado(lInicio, lInicio.altitud, 0)
  nodo = Nodo(None, eInicio, 0, 0, profundidad)
  contenedor.add(nodo)
  tiempos = []
  nNodos = 0
  try:
    nNodos = int(argv[1])-1
    cogidos = int(argv[2])
  except:
    sys.exit("Uso: python3 Generador.py <nNodosGenerar> <nNodosExtraer>")
  if(argv[1]<argv[2]):
    sys.exit("Se deben generar más nodos de los que se extraen")
  for i in range(nNodos+1):
    nodo = contenedor.get()
    profundidad = profundidad + 1
    sucesores = nodo.estado.sucesores()
    nodo = Nodo(nodo, sucesores[0], 0, 0, profundidad)
    valor = time.process_time()
    contenedor.add(nodo)
    tiempos.append(time.process_time()-valor)
  print("Añadidos " + str(contenedor.getLen()))
  print("media: "+str(media(tiempos)))
  print("maximo: "+str(maximo(tiempos)))
  print("minimo: "+str(minimo(tiempos)))
  print("total: " + str(total(tiempos)))
   
  tiempos = []
  for i in range(cogidos):
    valor = time.process_time()
    nodo = contenedor.extraer()
    tiempos.append(time.process_time() - valor)
  print("\nExtraídos " + str(cogidos))  
  print("media: "+str(media(tiempos)))
  print("maximo: "+str(maximo(tiempos)))
  print("minimo: "+str(minimo(tiempos)))
  print("total: " +str(total(tiempos)))

def media(lista):
  suma=0
  for i in lista:
    suma+=i
  return suma/len(lista)

def maximo(lista):
  maximo=0
  for i in lista:
    if i>maximo:
      maximo=i
  return maximo

def minimo(lista):
  minimo=float("inf")
  for i in lista:
    if i<minimo:
      minimo=i
  return minimo

def total(lista):
  total = 0
  for i in lista:
    total = total+i
  return total

main(sys.argv)
