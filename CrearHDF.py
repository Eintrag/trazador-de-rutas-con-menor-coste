#!/usr/bin/python3
#coding:utf-8
import glob
import h5py

def crearHDF():
  listaArchivos = glob.glob("datos/*.asc")
  hdf = h5py.File(('datos/mapas.hdf'), 'w')
  for nombreArchivo in listaArchivos: 
    archivo = open(nombreArchivo, 'r')
    #creacion, población del dataset y demostración de cómo acceder a su información
    #añadiendo un argumento más (chunks) puede dividir el dataset en chunks, pedazos
    #dset = f.create_dataset("chunked", (1000, 1000), chunks=(100, 100))

    NCOLS = leerNum(archivo.readline())
    NROWS = leerNum(archivo.readline())
    XLLCENTER = leerNum(archivo.readline())
    YLLCENTER = leerNum(archivo.readline())
    CELLSIZE = leerNum(archivo.readline())
    
    #crear archivo con la metainformación ^
    numeroHoja =  nombreArchivo.split('-')[1]
    meta = open('datos/' + numeroHoja+'.meta', 'w')
    print(meta)
    meta.write(NCOLS+' '+NROWS+' '+XLLCENTER+' '+YLLCENTER+' '+CELLSIZE)
    meta.close()
    
    archivo.readline() 
    print("creando dataset " + numeroHoja)
    dset = hdf.create_dataset(numeroHoja, (int(NROWS), int(NCOLS)))
    
    for i in range(0,int(NROWS)):
      linea = (archivo.readline()).rstrip()
      thisline = linea.split(" ")
      
      for j in range(0,int(NCOLS)):
        dset[i,j] = float(thisline[j])  
    archivo.close()
  hdf.close()
  print("Hojas creadas") 
  
def leerNum(cadena): #Extrae un numero de una linea
  num=""
  for a in cadena:
    if(ord(a)>47 and ord(a)<58):
      num = num + a
  return num