output=""
if [ "$3" = "-f" ]; then
  output="set term png; set output '$2$1.png'"
fi
if [ "$1" = "ruta" ]; then
  gnuplot -p -e "set format y '%7.0f'; ${output}; set key off; set xlabel 'X'; set ylabel 'Y'; set title 'Problema ${2%P*} Ruta'; plot '$2' using 1:2 with lines" 
elif [ "$1" = "alt" ]; then
  gnuplot -p -e "set ylabel 'Altitud'; set xlabel 'Pasos'; set key off; ${output}; set title 'Problema ${2%P*} Altitud'; plot '$2' using 3 with impulses"
else
  echo "Uso: ./plot.sh [ruta|alt] [archivoPlottable] [-f]
  -f = Crear archivo con la gráfica"
fi