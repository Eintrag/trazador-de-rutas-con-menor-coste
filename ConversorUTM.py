#coding:utf-8
import sys

def UTM(pag,x,y):
  archivo = open("datos/"+pag+".meta", 'r')
  listaDatos = archivo.readline().split(' ')
  for posicion in range(0,len(listaDatos)):
    listaDatos[posicion] = int(listaDatos[posicion])
  XLLCENTER = listaDatos[2]
  YLLCENTER = listaDatos[3]
  CELLSIZE = listaDatos[4]
  xutm=XLLCENTER+x*CELLSIZE
  yutm=YLLCENTER+y*CELLSIZE
  return (xutm, yutm, CELLSIZE)

'''
def test(argv):
  print("Testing: Introduzca pagina, X e Y para obtener las coordenadads UTM")
  print(UTM(argv[1], float(argv[2]), float(argv[3])))

test(sys.argv)
'''