from Estado import Estado
import math

class Nodo:
  padre = None
  estado = None
  valoracion = 0
  coste = 0
  desnivel = 0
  cSentido = 0
  profundidad = 0
  def __init__(self, padre, estado, coste, valoracion, profundidad):
    self.padre=padre
    self.estado=estado
    self.coste = coste
    self.valoracion = valoracion
    self.profundidad = profundidad
    if self.padre!=None:
      self.cSentido=padre.estado.acc==estado.acc
    else:
      self.cSentido=False
  def __eq__(self, other):
    if other is None:
      return False
      
    else:
      return self.estado==other.estado
  def __lt__(self, other):
    return self.valoracion<other.valoracion
  def __gt__(self, other):
    return self.valoracion>other.valoracion
  def __le__(self, other):
    return self.valoracion<=other.valoracion
  def __ge__(self, other):
    return self.valoracion>=other.valoracion
  def __str__(self):
    return str(self.estado)