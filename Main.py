#!/usr/bin/python3
#coding:utf-8
import sys
from Estado import Estado
from LugarGeografico import LugarGeografico
from Contenedor import Contenedor 
from Nodo import Nodo
from Buscador import Buscador
from CrearHDF import crearHDF
from ConversorUTM import UTM

def main(argv):
  if "-crear" in argv:
    print("Creando hdf...")
    try:
      crearHDF()
      sys.exit("Creación completa, ya puede usar los mapas.")
    except ValueError as err:
      print(err)
      sys.exit("No se pudieron crear los hdf.")
  else:
    print("Para crear los hdf ejecute el programa con la opción -crear")

  if "-esquinas" in argv:
    #Testing
    dameLasEsquinas()
    sys.exit()
    
  elif (len(argv) == 3):
    #Testing
    lugar = LugarGeografico(argv[1], argv[2])
    print ("Altitud= " + str(lugar.altitud()) + "\n" + str(lugar.LLG))
    sys.exit()
    
  else:  
    try: 
      inicioX = argv[1]  
      inicioY = argv[2]
      destinoX = argv[3] 
      destinoY = argv[4]
      estrategia = argv[5]
      profundidad = argv[6]
      archivo = argv[7]
    except:
      print("Uso: python3 Main.py [origenX] [origenY] [destinoX] [destinoY] [estrategia] [profundidad] [archivo]")
      print("Coordenadas UTM para origen y destino")
      print("[estrategia] = A, profundidad, anchura, uniforme y voraz")
      print("[archivo] = Nombre del archivo donde guardar el resultado")
      
    try:  
      lugarInicio = LugarGeografico(inicioX, inicioY)
      lugarDestino = LugarGeografico(destinoX, destinoY)
      if (str(lugarInicio)=="[]" or str(lugarDestino)=="[]"):
        print("El Inicio o el Destino no está en el mapa")
      else:
        iniArchivo((inicioX,inicioY),lugarInicio, (destinoX, destinoY),lugarDestino, archivo)
        resolver(lugarInicio, lugarDestino, estrategia, profundidad, archivo)  
    except ValueError as err:
      print(err)

def resolver(inicio, destino, estrategia, profundidad, archivo):
  #Recibe inicio y destino como LugaresGeograficos
  origen = Estado(inicio, inicio.altitud(), None)
  meta = Estado(destino, destino.altitud(), None)
  print("Inicio: " + str(inicio))
  print("Destino: " + str(destino))
  
  resultado=[]
  busqueda=Buscador()
  
  #se obtiene la lista de estados
  resultado=busqueda.busqueda(origen,meta,estrategia,int(profundidad))
  #almacena el resultado en un archivo
  guardar(resultado, busqueda.nNodos(), busqueda.tiempo, archivo)
      
def CreaListaNodosArbol(LS, nodo_actual, estrategia):
  if(estrategia == 'profundidad'):
    print("profundidad")
    
def iniArchivo(ini, lini, dest, ldest, rutaArchivo):
  archivo = open(rutaArchivo, "w")
  archivo.write("#"+rutaArchivo)
  archivo.write("\n#Inicio: "+str(ini)+" "+str(lini)+" Altitud: "+str(lini.altitud()))
  archivo.write("\n#Destino: "+str(dest)+" "+str(ldest)+" Altitud: "+str(ldest.altitud()))
  
def guardar(listaNodos, generados, tiempo, rutaArchivo):
  archivo = open(rutaArchivo,"a")
  if len(listaNodos)>0:
    archivo.write("\n#Coste: "+str(listaNodos[-1].coste)+"\t Profundidad: "+str(listaNodos[-1].profundidad)+"\n#Nodos Generados: "+str(generados)+"\t Tiempo: "+str(round(tiempo,3))+" segundos\n")
    archivo.write("#UTMX\tUTMY\tAltitud\tLLG\n")
    for n in listaNodos:
      utm = UTM(n.estado.lugar.hoja, n.estado.lugar.x, n.estado.lugar.y)
      archivo.write(str(utm[0])+"\t"+str(utm[1])+"\t"+str(n.estado.altitud)+"\t" + str(n.estado.lugar)+"\n")
  else:
    archivo.write("\n#Nodos Generados: "+str(generados)+"\t Tiempo: "+str(round(tiempo,3))+" segundos")
    archivo.write("\n#No se ha encontrado ningun camino.\n")   
  archivo.close()

def dameLasEsquinas():
  #Testing
  print("-------------------------------------------")
  print("0711")
  print("-------------------------------------------")
  a0711 = [(397400, 4353800), (397400, 4373200), (426800, 4373200), (426800, 4353800)]
  for par in a0711:
    lugarInicio = LugarGeografico(par[0], par[1])
    print("UTM: " + str(par[0]),str(par[1]))
    print("altitud = " + str(lugarInicio.altitud()) + "\tLLG = " +  str(lugarInicio))  
  print("-------------------------------------------")
  print("0712")
  print("-------------------------------------------")
  a0712 = [(426000, 4353600),(426000, 4373000), (455600, 4373000), (455600, 4353600)]
  for par in a0712:
    lugarInicio = LugarGeografico(par[0], par[1])
    print("UTM: " + str(par[0]),str(par[1]))
    print("altitud = " + str(lugarInicio.altitud()) + "\tLLG = " +  str(lugarInicio))
  print("-------------------------------------------")
  print("0736")
  print("-------------------------------------------")
  a0736 = [(397200, 4335400), (397200, 4354800), (426600, 4354800), (426600, 4335400)]
  for par in a0736:
    lugarInicio = LugarGeografico(par[0], par[1])
    print("UTM: " + str(par[0]),str(par[1]))
    print("altitud = " + str(lugarInicio.altitud()) + "\tLLG = " + str(lugarInicio))
  print("-------------------------------------------")
  print("0737")
  print("-------------------------------------------")
  a0737 = [(426000, 4335200), (426000, 4354400), (455400, 4354400), (455400, 4335200)]
  for par in a0737:
    lugarInicio = LugarGeografico(par[0], par[1])
    print("UTM: " + str(par[0]),str(par[1]))
    print("altitud = " + str(lugarInicio.altitud()) + "\tLLG = " +  str(lugarInicio))
    
main(sys.argv)
