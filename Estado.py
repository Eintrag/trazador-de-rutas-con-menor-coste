#coding:utf-8
from ConversorUTM import UTM
from LugarGeografico import LugarGeografico
import math
class Estado:
  lugarGeografico = None
  altitud = 0
  acc = {}
  def __init__ (self, lugar, altitud, acc):
    self.lugar=lugar
    self.altitud=altitud
    self.acc=acc
  def sucesores(self):
    '''
    Codigo para la direccion empezando por el ESTE, 
    en sentido opuesto a las agujas del reloj
    '''
    (coordX, coordY, CELLSIZE) = UTM(self.lugar.hoja,self.lugar.x,self.lugar.y)
    sucesores=[]
    direcciones = {}
    direcciones['E'] = (CELLSIZE + coordX, coordY)
    direcciones['NE'] = (CELLSIZE + coordX, -CELLSIZE + coordY)
    direcciones['N'] = (coordX, -CELLSIZE + coordY)
    direcciones['NO'] = (-CELLSIZE + coordX, -CELLSIZE + coordY)
    direcciones['O'] = (-CELLSIZE + coordX, coordY)
    direcciones['SO'] = (-CELLSIZE + coordX, CELLSIZE + coordY)
    direcciones['S'] = (coordX, CELLSIZE + coordY)
    direcciones['SE'] = (CELLSIZE + coordX, CELLSIZE + coordY)
    
    ldir = ["E", "NE", "N", "NO", "O", "SO", "S", "SE"]
    
    for d in ldir:
      despl = direcciones[d]
      succLugar = LugarGeografico(despl[0], despl[1])
      if succLugar.altitud()!= None:
        sucesores.append(Estado(succLugar, succLugar.altitud(), d))
      
    return sucesores
  
  def __eq__(self, otro):
    return otro.lugar==self.lugar
  
  def __str__(self):
    return str(self.lugar)