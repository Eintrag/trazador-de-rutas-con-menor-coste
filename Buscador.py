#coding:utf-8
import math
import time
from Estado import Estado
from LugarGeografico import LugarGeografico
from Contenedor import Contenedor 
from Nodo import Nodo
from ConversorUTM import UTM
import sys
class Buscador:
  profundidad=0
  tiempo=0
  def init(self, inicio, destino):
    self.frontera = Contenedor()
    n_inicial = Nodo(None, inicio, 0, 0, 0)
    self.frontera.add(n_inicial)
    self.solucion = False
    self.visitados={}
  def busqueda(self, inicio, destino, estrategia, max_prof):
    #Bucle de busqueda
    self.tiempo=time.process_time()
    valor=0
    self.init(inicio, destino)   
    self.visitados={}
    print("iniciar busqueda")
    while not self.solucion and not self.frontera.getLen()==0:
      sys.stdout.flush()
      n_actual=self.frontera.extraer()
      if destino==n_actual.estado:
        self.solucion = True
      elif n_actual.profundidad<max_prof and str(n_actual) not in self.visitados:
        listSucc=n_actual.estado.sucesores()
        #print("-",end=" ")
        #print(str(n_actual.estado.lugar.x)+" "+str(n_actual.estado.lugar.y)+" "+str(n_actual.estado.lugar.altitud()))
        #print("->"+str([s.valoracion for s in listSucc]))
        
        if estrategia=="profundidad":
          valor=self.estrategiaProfundidad(n_actual, listSucc, valor)
        elif estrategia=="anchura":
          valor=self.estrategiaAnchura(n_actual, listSucc, valor)
        elif estrategia=="uniforme":
          self.estrategiaUniforme(n_actual, listSucc)
        elif estrategia=="voraz":
          self.estrategiaVoraz(n_actual, listSucc, destino)
        elif estrategia=="A":
          self.estrategiaA(n_actual, listSucc, destino)
      self.visitados[str(n_actual)]=n_actual
      #print(len(self.frontera.lista))
      if n_actual.profundidad+1>self.profundidad:
        self.profundidad=n_actual.profundidad+1
 
    self.tiempo=time.process_time()-self.tiempo
    if self.solucion:
      
      return self.crear_solucion(n_actual)
    else:
      return []
  
  def estrategiaProfundidad(self, n_actual, listSucc, valor):
    valoracion=valor
    for succ in listSucc:
      nodo=Nodo(n_actual, succ, n_actual.coste+self.coste(n_actual.estado, succ), valoracion, n_actual.profundidad+1)
      if str(nodo) not in self.visitados:
        self.frontera.add(nodo)
        valoracion-=1
    return valoracion
  
  def estrategiaAnchura(self,n_actual, listSucc, valor):
    valoracion=valor
    for succ in listSucc:
      nodo=Nodo(n_actual, succ, n_actual.coste+self.coste(n_actual.estado,succ), valoracion, n_actual.profundidad+1)
      if str(nodo) not in self.visitados:
        #print(nodo.valoracion)
        self.frontera.add(nodo)
        valoracion+=1
    return valoracion
  
  def estrategiaUniforme(self,n_actual,listSucc):
    for succ in listSucc:
      coste=self.coste(n_actual.estado,succ)
      nodo=Nodo(n_actual, succ, n_actual.coste+coste, n_actual.coste+coste, n_actual.profundidad+1)
      if str(nodo) not in self.visitados:
        self.frontera.add(nodo)
    
  def estrategiaVoraz(self,n_actual, listSucc, destino):
    for succ in listSucc:
      nodo=Nodo(n_actual, succ, n_actual.coste+self.coste(n_actual.estado, succ), self.heuristica(succ, destino), n_actual.profundidad+1)
      if str(nodo) not in self.visitados and nodo not in self.frontera.lista:
        self.frontera.add(nodo)
      else:
        None
      
  def estrategiaA(self,n_actual,listSucc, destino):
    for succ in listSucc:
      coste=self.coste(n_actual.estado,succ)
      heuristica=self.heuristica(succ, destino)
      nodo=Nodo(n_actual, succ, n_actual.coste+coste, n_actual.coste+coste+heuristica, n_actual.profundidad+1)
      if str(nodo) not in self.visitados:
        self.frontera.add(nodo)      
    
  def coste(self,origen, destino):
    ld=25
    if destino.acc in ["NE","NO","SE","SO"]:
      ld=25*math.sqrt(2)
    inclAlt=math.fabs(origen.altitud-destino.altitud)
    longitud=math.sqrt((ld*ld)+(inclAlt*inclAlt))
    if destino.acc!=origen.acc:
      return longitud*1.10
    else:
      return longitud
    
  def heuristica(self,origen, meta):
    #Hacer los cálculos directamente  sobre las coord UTM
    (xn, yn, hoja)=UTM(origen.lugar.hoja,origen.lugar.x,origen.lugar.y)
    (xd, yd, hoja)=UTM(meta.lugar.hoja,meta.lugar.x,meta.lugar.y)
    
    return math.sqrt(((xd-xn)*(xd-xn))+((yd-yn)*(yd-yn)))
  def nNodos(self):
    return len(self.visitados)+len(self.frontera.lista)
  def crear_solucion(self,final):
    listaSolucion=[]
    n_actual=final
    while n_actual!=None:
     #print(self.heuristica(n_actual.estado, final.estado))
     #print(n_actual.coste)
     listaSolucion.insert(0,n_actual)
     n_actual=n_actual.padre
    return listaSolucion