#coding:utf-8
import glob
from ConversorUTM import UTM
import h5py
import Util

class LugarGeografico:
  hoja = ""
  x = 0
  y = 0
  rows=0
  cols=0
  LLG = []
  def __init__(self,umtX, umtY):
    self.LLG = []
    #vamos a revisar los archivos .meta para ver en cual se encuentra nuestra coordenada.
    listaArchivos = glob.glob("datos/*.meta")
    for nombreArchivo in listaArchivos: 
      archivo = open(nombreArchivo, 'r')
      listaDatos = archivo.readline().split(' ')
      for posicion in range(0,len(listaDatos)):
        listaDatos[posicion] = int(listaDatos[posicion])
      
      NCOLS = listaDatos[0]
      NROWS = listaDatos[1]
      XLLCENTER = listaDatos[2]
      YLLCENTER = listaDatos[3]
      CELLSIZE = listaDatos[4]
      
      #dividimos las coordenadas entre CELLSIZE para trabajar con celdas de la matriz
      #Estas son las coordenadas en UMT de las zonas representadas por la matriz
      xinf=XLLCENTER-(CELLSIZE/2)
      xsup=XLLCENTER+NCOLS*CELLSIZE+CELLSIZE/2
      yinf=YLLCENTER-(CELLSIZE/2)
      ysup=YLLCENTER+NROWS*CELLSIZE+CELLSIZE/2
      umtX = float(umtX)
      umtY = float(umtY)
      #obtiene la hoja y coordenadas donde se encuentra la posición UMT
      
      
      if(umtX<=xsup) and (umtX>=xinf) and (umtY>=yinf) and (umtY<=ysup):
        self.hoja = nombreArchivo.split('/')[1].split('.')[0]
        self.x=int((umtX-xinf)/CELLSIZE)
        self.y=int((umtY-yinf)/CELLSIZE)
        self.rows=NROWS
        self.cols=NCOLS
        self.LLG.append((self.hoja, self.x, self.y))
        
  def altitud(self):
    try:
      hdf = h5py.File(('datos/mapas.hdf'), 'r')
      return round(float(hdf.get(self.hoja)[self.rows-1-self.y][self.x]),2)   
    except:
      pass
      
  def __eq__(self, other):
    if other is None:
      return False
    else:
      #return self.LLG == other.LLG
      return Util.elementoComun(self.LLG, other.LLG)
    
  def __str__(self):
    return str(self.LLG)